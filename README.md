# leetcode_session
Welcome everyone  
The session was for those who want to practice leetcode

# How it goes ?
* English only
* once every week or every two weeks (it depends)
* video conference style (hangout予定)

# Rules
* on-line coding
* no ide support
* 1 question for each participator, 30 mins
* any lang is okay, prefer c or c++
* compilable code
* explain -> coding -> complexity -> optimize

# Event 予定 - 2020.5-2020.7
* 2020.05.02 10:30-12:00 Taiwan time
* 2020.05.09 10:30-12:00 Taiwan time
* 2020.05.16 10:30-12:00 Taiwan time
* 2020.05.23 10:30-12:00 Taiwan time
* 2020.05.30 10:30-12:00 Taiwan time
* 2020.06.06 10:30-12:00 Taiwan time
* 2020.06.13 10:30-12:00 Taiwan time
* 2020.06.20 10:30-12:00 Taiwan time
* 2020.06.27 10:30-12:00 Taiwan time
* 2020.07.04 10:30-12:00 Taiwan time
* 2020.07.11 10:30-12:00 Taiwan time
* 2020.07.18 10:30-12:00 Taiwan time
* 2020.07.25 10:30-12:00 Taiwan time


# members
| name  | note |
| ------ | ------ |
| gem | host |
| allen |  | 
| roger |  |
| brian |  | 
